<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Menu
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/salas') }}">
                        Salas
                    </a>
                    <a href="{{ url('/reserva/reservas') }}">
                        Reservas
                    </a>
                    <a href="{{ url('/horarios/horarios') }}">
                        Horários
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
