<div class="form-group {{ $errors->has('hora_inicio') ? 'has-error' : ''}}">
    {!! Form::label('hora_inicio', 'Hora Inicio', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('hora_inicio', null, ['class' => 'form-control']) !!}
        {!! $errors->first('hora_inicio', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('hora_fim') ? 'has-error' : ''}}">
    {!! Form::label('hora_fim', 'Hora Fim', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('hora_fim', null, ['class' => 'form-control']) !!}
        {!! $errors->first('hora_fim', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
