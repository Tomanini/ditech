@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Horarios</div>
                    <div class="panel-body">
                        <a href="{{ url('/horarios/horarios/create') }}" class="btn btn-success btn-sm" title="Add New Horario">
                            <i class="fa fa-plus" aria-hidden="true"></i> Adicionar novo
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/horarios/horarios', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            {{--<input type="text" class="form-control" name="search" placeholder="Buscar...">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<button class="btn btn-default" type="submit">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                {{--</button>--}}
                            {{--</span>--}}
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Hora Inicio</th><th>Hora Fim</th><th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($horarios as $item)
                                    <tr>
                                        <td>{{ $item->hora_inicio }}</td><td>{{ $item->hora_fim }}</td>
                                        <td>
                                            <a href="{{ url('/horarios/horarios/' . $item->id) }}" title="View Horario"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Ver</button></a>
                                            <a href="{{ url('/horarios/horarios/' . $item->id . '/edit') }}" title="Edit Horario"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/horarios/horarios', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Apagar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Horario',
                                                        'onclick'=>'return confirm("Apagar horario?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $horarios->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
