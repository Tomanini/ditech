@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Reservas</div>
                    <div class="panel-body">
                        <a href="{{ url('/reserva/reservas/create') }}" class="btn btn-success btn-sm" title="Add New Reserva">
                            <i class="fa fa-plus" aria-hidden="true"></i> Criar nova reserva
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/reserva/reservas', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            {{--<input type="text" class="form-control" name="search" placeholder="Search...">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<button class="btn btn-default" type="submit">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                {{--</button>--}}
                            {{--</span>--}}
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Usuário</th><th>Sala</th></th><th>Hora</th><th>Data</th><th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($reservas as $item)
                                    <tr>
                                        {{--<td>{{ $item->id }}</td>--}}
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->sala->nome }}</td>
                                        <td>{{ $item->horarios->hora_inicio . " até " . $item->horarios->hora_fim }}</td>
                                        <td>{{ $item->data }}</td>
                                        <td>
                                            <a href="{{ url('/reserva/reservas/' . $item->id) }}" title="View Reserva"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Ver</button></a>
                                            <a href="{{ url('/reserva/reservas/' . $item->id . '/edit') }}" title="Edit Reserva"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/reserva/reservas', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Apagar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Reserva',
                                                        'onclick'=>'return confirm("Deseja apagar a reserva?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{--<div class="pagination-wrapper"> {!! $reservas->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
