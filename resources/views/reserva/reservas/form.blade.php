<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
{{--    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}--}}
    <div class="col-md-6">
        <input type="hidden" name="user_id" value="<?php echo $user->id; ?>"></input>
    </div>
</div>
<div class="form-group {{ $errors->has('sala_id') ? 'has-error' : ''}}">
    {!! Form::Label('salas', 'Sala:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control" name="sala_id">
            @foreach($salas as $sala)
                <option value="{{$sala->id}}">{{$sala->nome}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('horarios_id') ? 'has-error' : ''}}">
        {!! Form::Label('horarios', 'Horário:', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control" name="horarios_id">
                @foreach($horarios as $item)
                    <option value="{{$item->id}}">{{$item->hora_inicio . " até " . $item->hora_fim}}</option>
                @endforeach
            </select>
        </div>
</div>
<div class="form-group {{ $errors->has('data') ? 'has-error' : ''}}">
    {!! Form::label('data', 'Data', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('data', null, ['class' => 'form-control']) !!}
        {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
