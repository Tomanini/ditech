<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reservas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'horarios_id', 'sala_id', 'data'];

    public function sala(){
        return $this->belongsTo('App\Sala');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function horarios(){
        return $this->belongsTo('App\Horario');
    }

    
}
