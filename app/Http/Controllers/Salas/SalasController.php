<?php

namespace App\Http\Controllers\Salas;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Sala;
use Illuminate\Http\Request;
use Session;

class SalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $salas = Sala::where('nome', 'LIKE', "%$keyword%")
				->orWhere('descricao', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $salas = Sala::paginate($perPage);
        }

        return view('salas.salas.index', compact('salas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('salas.salas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Sala::create($requestData);

        Session::flash('flash_message', 'Sala added!');

        return redirect('salas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sala = Sala::findOrFail($id);

        return view('salas.salas.show', compact('sala'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sala = Sala::findOrFail($id);

        return view('salas.salas.edit', compact('sala'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $sala = Sala::findOrFail($id);
        $sala->update($requestData);

        Session::flash('flash_message', 'Sala updated!');

        return redirect('salas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Sala::destroy($id);

        Session::flash('flash_message', 'Sala deleted!');

        return redirect('salas');
    }
}
