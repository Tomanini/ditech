<?php

namespace App\Http\Controllers\Reserva;

use App\Horario;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Reserva;
use App\Sala;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Session;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $reservas = Reserva::all();
//        $reservas = Reserva::paginate($perPage);
//        if (!empty($keyword)) {
//            $reservas = Reserva::where('user_id', 'LIKE', "%$keyword%")
//				->orWhere('horarios_id', 'LIKE', "%$keyword%")
//				->orWhere('data', 'LIKE', "%$keyword%")
//				->paginate($perPage);
//        } else {
//            $reservas = Reserva::paginate($perPage);
//        }

        return view('reserva.reservas.index', compact('reservas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('reserva.reservas.create', ['horarios' => Horario::all(), 'salas' => Sala::all(), 'user' => Auth::user()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $verificaReserva = Reserva::where('sala_id', '=', $requestData['sala_id'])
                                    ->where('horarios_id', '=', $requestData['horarios_id'])
                                    ->where('data', '=', $requestData['data'])
                                    ->get();
        if(count($verificaReserva)){
//            echo "ja tem reserva";
        }else{
            Reserva::create($requestData);
            Session::flash('flash_message', 'Reserva added!');
            return redirect('reserva/reservas');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $reserva = Reserva::findOrFail($id);

        return view('reserva.reservas.show', compact('reserva'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $reserva = Reserva::findOrFail($id);


//        return view('reserva.reservas.edit', compact('reserva'));
        return view('reserva.reservas.edit', ['reserva' => $reserva, 'horarios' => Horario::all(), 'salas' => Sala::all(), 'user' => Auth::user()]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $reserva = Reserva::findOrFail($id);
        $reserva->update($requestData);

        Session::flash('flash_message', 'Reserva updated!');

        return redirect('reserva/reservas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Reserva::destroy($id);

        Session::flash('flash_message', 'Reserva deleted!');

        return redirect('reserva/reservas');
    }
}
