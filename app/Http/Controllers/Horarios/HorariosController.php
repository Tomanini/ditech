<?php

namespace App\Http\Controllers\Horarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Horario;
use Illuminate\Http\Request;
use Session;

class HorariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $horarios = Horario::where('hora_inicio', 'LIKE', "%$keyword%")
				->orWhere('hora_fim', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $horarios = Horario::paginate($perPage);
        }

        return view('horarios.horarios.index', compact('horarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('horarios.horarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Horario::create($requestData);

        Session::flash('flash_message', 'Horario added!');

        return redirect('horarios/horarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $horario = Horario::findOrFail($id);

        return view('horarios.horarios.show', compact('horario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $horario = Horario::findOrFail($id);

        return view('horarios.horarios.edit', compact('horario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $horario = Horario::findOrFail($id);
        $horario->update($requestData);

        Session::flash('flash_message', 'Horario updated!');

        return redirect('horarios/horarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Horario::destroy($id);

        Session::flash('flash_message', 'Horario deleted!');

        return redirect('horarios/horarios');
    }
}
