<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::resource('salas', 'Salas\\SalasController');
Route::resource('horarios/horarios', 'Horarios\\HorariosController');
//Route::get('reservas/reservas', 'Reservas\\ReservasController');
Route::resource('reservas/reservas', 'Reservas\\ReservasController');
Route::resource('reserva/reservas', 'Reserva\\ReservasController');