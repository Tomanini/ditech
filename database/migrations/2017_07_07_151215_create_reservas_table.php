<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('sala_id');
            $table->integer('horarios_id');
            $table->date('data');
            $table->timestamps();
        });

        Schema::table('reservas', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('horarios_id')->references('id')->on('horarios');
            $table->foreign('sala_id')->references('id')->on('salas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservas');
    }
}
